
import java.util.*;

//http://www.i-programmer.info/programming/theory/3458-brackets-are-trees.html
//https://stackoverflow.com/questions/7866587/create-a-binary-tree-from-an-algebraic-expression
//https://stackoverflow.com/questions/4589951/parsing-an-arithmetic-expression-and-building-a-tree-from-it-in-java
public class TreeNode {

	private String name;
	private TreeNode firstChild;
	private TreeNode nextSibling;

	TreeNode(String n, TreeNode d, TreeNode r) {
		name = n;
		firstChild = d;
		nextSibling = r;
	}

	public static TreeNode parsePrefix(String s) {
		if (s.contains(",,")) {
			throw new RuntimeException("Ei saa olla kaks koma: " + s);
		} else if (s.contains("((")) {
			throw new RuntimeException("Liiga palju sulge: " + s);
		} else if (s.contains("()")) {
			throw new RuntimeException("T�hi puu: " + s);
			// https://stackoverflow.com/questions/5051194/detecting-tab-space-and-next-lime-markup-symbols-in-text-files
		} else if (s.contains("\t")) {
			throw new RuntimeException("Liigne tab: " + s);
		} else if (s.contains(" ")) {
			throw new RuntimeException("Liigne t�hik v�i t�hi: " + s);
		} else if (s.contains(",") && (!s.contains("(") || !s.contains(")"))) {
			throw new RuntimeException("Sulud puudu: " + s);
		} else if (s.startsWith(")")) {
			throw new RuntimeException("Ei saa alata ')' suluga " + s);
		} else if (s.startsWith("(")) {
			throw new RuntimeException("Ei saa alata '(' suluga " + s);
		}
		int count = s.replace("(", "").length() - s.replace(")", "").length();
		if (count != 0) {
			throw new RuntimeException("Pole v�rdne arv sulge " + s);
		}

		TreeNode treeNode = new TreeNode(null, null, null);
		String[] jupid = s.split("");
		Stack<TreeNode> stack = new Stack<TreeNode>();
		boolean root = false;
		for (int i = 0; i < jupid.length; i++) {
			if (root == true) {
				throw new RuntimeException("Liiga palju elemente: " + s);
			}
			String jupp = jupid[i];
			if (jupp.equals("(")) {
				stack.push(treeNode);
				treeNode.firstChild = new TreeNode(null, null, null);
				treeNode = treeNode.firstChild;
				if (jupid[i + 1].equals(",")) {
					throw new RuntimeException("Peale sulgu ei saa olla koma " + s);
				}
			} else if (jupp.equals(",")) {
				if (root == true) {
					throw new RuntimeException("Liiga palju elemente: " + s);
				}
				treeNode.nextSibling = new TreeNode(null, null, null);
				treeNode = treeNode.nextSibling;

			} else if (jupp.equals(")")) {
				treeNode = stack.pop();

				if (stack.isEmpty()) {
					root = true;
				} else {
					if (jupid[i + 1].matches("[*a-zA-Z]") || jupid[i + 1].matches("[*0-9]")  || jupid[i + 1].matches("[(]")) {
						throw new RuntimeException("Peale sulgu puudub koma " + s);
					}if (jupid[i].equals(",") && i<jupid.length-1 && (jupid[i+1].matches("[(]"))) {
			            throw new RuntimeException("Vigane avaldis " + s );
			         }
				}
			} else {
				if (treeNode.name == null) {
					treeNode.name = jupp;
				} else {
					treeNode.name = treeNode.name + jupp;
				}
			}
			if (jupid[i].equals(",") && i<jupid.length-1 && (jupid[i+1].matches("[(]"))) {
	            throw new RuntimeException("Vigane avaldis " + s );
	         }
		}
		return treeNode;
	}

	public String rightParentheticRepresentation() {
		StringBuffer b = new StringBuffer();
		
		if (firstChild != null) {
			b.append("(");
			b.append(firstChild.rightParentheticRepresentation());
			b.append(")");
		}
		if (nextSibling != null) {
			b.append(name);
			b.append(",");
			b.append(nextSibling.rightParentheticRepresentation());
		} else {
			b.append(name);
		}
	
		return b.toString();
	}

	public static void main(String[] param) {
		String s = "A(B1,C,D)";
		String a = "+(*(-(5,1),7),/(6,3))";
		String q = "ABC(3,55(gg),5(fff),4)";
		TreeNode t = TreeNode.parsePrefix(s);
		TreeNode d = TreeNode.parsePrefix(a);
		TreeNode r = TreeNode.parsePrefix(q);
		String v = t.rightParentheticRepresentation();
		String e = d.rightParentheticRepresentation();
		String f = r.rightParentheticRepresentation();
		System.out.println(s + " ==> " + v); // A(B1,C,D) ==> (B1,C,D)A
		System.out.println(a + " ==> " + e);
		System.out.println(q + " ==> " + f);
	}
}
